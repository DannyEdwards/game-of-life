const cssWidthValue = getComputedStyle(document.body).getPropertyValue(
  "--width",
);

const WIDTH = parseInt(cssWidthValue);

type LifeArray = boolean[];

/** Returns neighbours of cell `index` belonging to `array`. */
const getLiveNeighbours = (array: LifeArray, index: number) => {
  const bottom = WIDTH + 1;
  const top = WIDTH - 1;
  return [
    array[index - bottom],
    array[index - WIDTH],
    array[index - top],
    array[index - 1],
    array[index + 1],
    array[index + bottom],
    array[index + WIDTH],
    array[index + top],
  ].filter(Boolean).length;
};

/** Implements rules of Conway's Game of Life */
const evolve = (generation: LifeArray) =>
  generation.map(function (cell, index) {
    const liveNeighbours = getLiveNeighbours(this, index);
    // Any live cell with two or three neighbors survives.
    if (cell && (liveNeighbours === 2 || liveNeighbours === 3)) return true;
    // Any dead cell with three live neighbors becomes a live cell.
    if (!cell && liveNeighbours === 3) return true;
    // All other live cells die in the next generation. Similarly, all other dead cells stay dead.
    return false;
  }, generation);

const STATE_MAP = new Map<boolean, "live" | "dead">([
  [true, "live"],
  [false, "dead"],
]);

/** Paints array srate to DOM with <ol>s and <li>s. */
const draw = (array: LifeArray) =>
  array.map(
    (state, index) =>
      (document.getElementById(String(index))!.className =
        STATE_MAP.get(state)!),
  );

let life: LifeArray = [...Array(Math.pow(WIDTH, 2)).fill(false)];
const space = document.querySelector(".space")!;
const playButton = document.querySelector("input")!;

const time =
  (generation = 0) =>
  () => {
    generation++;
    life = evolve(life);
    draw(life);
    console.log(`Generation ${generation}`);
  };

const RATE = 128;

const handleButtonClick = (interval?: number) => (event: Event) => {
  const target = event.target as HTMLInputElement;
  if (!target.checked) clearInterval(interval);
  else interval = setInterval(time(), RATE);
};

const handleCellClick = (event: Event) => {
  const target = event.target as HTMLInputElement;
  if (target.localName !== "td") return;
  life[target.id] = !life[target.id];
  target.className = STATE_MAP.get(life[target.id])!;
};

space.addEventListener("mousedown", handleCellClick);
playButton.addEventListener("click", handleButtonClick());

/** Initial draw */
const bang = (row?: HTMLElement) => (array: LifeArray) => {
  array.map((state, index) => {
    const cell = document.createElement("td");
    cell.className = STATE_MAP.get(state)!;
    cell.id = String(index);
    if (index % WIDTH === 0) {
      row = document.createElement("tr");
      space.appendChild(row);
    }
    row?.appendChild(cell);
  });
};

// And then there was light.
bang()(life);
